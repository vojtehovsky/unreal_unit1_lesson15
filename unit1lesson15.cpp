#include <iostream>

void printNumbers(int maxValue, bool isEven) {
    for (int i = 0; i <= maxValue; i++) {
        bool shouldPrint = (isEven && i % 2 == 0) || (!isEven && i % 2 != 0);
        if (shouldPrint) {
            std::cout << i << " ";
        }
    }
}

int main() {
    int N = 23;
    printNumbers(N, true);
}
